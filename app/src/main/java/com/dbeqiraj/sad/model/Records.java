package com.dbeqiraj.sad.model;

import java.io.Serializable;

public class Records implements Serializable {
    private RecordsRecords records;

    public RecordsRecords getRecords() {
        return this.records;
    }

    public void setRecords(RecordsRecords records) {
        this.records = records;
    }
}
