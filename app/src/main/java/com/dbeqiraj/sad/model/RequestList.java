package com.dbeqiraj.sad.model;

public class RequestList {
    private String ReportName;
    private int MonthID;
    private String ProductCode;
    private int UserID;
    private int Days;
    private int ClientID;
    private String From;
    private String To;

    public String getReportName() {
        return this.ReportName;
    }

    public void setReportName(String ReportName) {
        this.ReportName = ReportName;
    }

    public int getMonthID() {
        return this.MonthID;
    }

    public void setMonthID(int MonthID) {
        this.MonthID = MonthID;
    }

    public String getProductCode() {
        return this.ProductCode;
    }

    public void setProductCode(String ProductCode) {
        this.ProductCode = ProductCode;
    }

    public int getUserID() {
        return this.UserID;
    }

    public void setUserID(int UserID) {
        this.UserID = UserID;
    }

    public int getDays() {
        return this.Days;
    }

    public void setDays(int Days) {
        this.Days = Days;
    }

    public int getClientID() {
        return this.ClientID;
    }

    public void setClientID(int ClientID) {
        this.ClientID = ClientID;
    }

    public String getFrom() {
        return this.From;
    }

    public void setFrom(String From) {
        this.From = From;
    }

    public String getTo() {
        return this.To;
    }

    public void setTo(String To) {
        this.To = To;
    }
}
