package com.dbeqiraj.sad.model;

public class RecordsRecords {
    private RecordsRecordsEcuriaArtikullit[] EcuriaArtikullit;

    public RecordsRecordsEcuriaArtikullit[] getEcuriaArtikullit() {
        return this.EcuriaArtikullit;
    }

    public void setEcuriaArtikullit(RecordsRecordsEcuriaArtikullit[] EcuriaArtikullit) {
        this.EcuriaArtikullit = EcuriaArtikullit;
    }
}
