package com.dbeqiraj.sad.model;

public class RecordsRecordsEcuriaArtikullit {
    private String dt;
    private int cntKlient;
    private String KArt;

    public String getDt() {
        return this.dt;
    }

    public void setDt(String dt) {
        this.dt = dt;
    }

    public int getCntKlient() {
        return this.cntKlient;
    }

    public void setCntKlient(int cntKlient) {
        this.cntKlient = cntKlient;
    }

    public String getKArt() {
        return this.KArt;
    }

    public void setKArt(String KArt) {
        this.KArt = KArt;
    }
}
