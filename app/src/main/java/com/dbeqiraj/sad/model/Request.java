package com.dbeqiraj.sad.model;

import java.io.Serializable;

public class Request implements Serializable{
    private RequestList[] List;

    public RequestList[] getList() {
        return this.List;
    }

    public void setList(RequestList[] List) {
        this.List = List;
    }
}
