package com.dbeqiraj.sad.model;

import java.io.Serializable;

/**
 * Created by d.beqiraj on 5/1/2017.
 */

public class TokenResponse implements Serializable {

    private String Link;
    private String Key;

    public TokenResponse(String link, String key) {
        Link = link;
        Key = key;
    }

    public String getLink() {
        return Link;
    }

    public void setLink(String link) {
        Link = link;
    }

    public String getKey() {
        return Key;
    }

    public void setKey(String key) {
        Key = key;
    }
}
