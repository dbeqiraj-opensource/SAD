package com.dbeqiraj.sad.activities.details;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.dbeqiraj.sad.R;
import com.dbeqiraj.sad.activities.details.fragments.Chart;
import com.dbeqiraj.sad.activities.details.fragments.Table;
import com.dbeqiraj.sad.adapters.DetailsAdapter;
import com.dbeqiraj.sad.utilities.Utils;

public class Details extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        if ( getSupportActionBar() != null ) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setElevation(0);
        }

        CharSequence[] codes = Utils.getCodes(this);

        Bundle bundle = new Bundle();
        bundle.putString("code", codes[0].toString());
        bundle.putCharSequenceArray("codes", codes);
        Chart chart = new Chart();
        Table table = new Table();
        chart.setArguments(bundle);
        table.setArguments(bundle);
        DetailsAdapter adapter = new DetailsAdapter(getSupportFragmentManager());
        adapter.addFragment(chart, getString(R.string.chart));
        adapter.addFragment(table, getString(R.string.table));

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        setTitle(String.format(getString(R.string.title_details), codes[0]));
    }
}
