package com.dbeqiraj.sad.activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.dbeqiraj.sad.R;
import com.dbeqiraj.sad.utilities.Utils;
import com.dbeqiraj.sad.utilities.ws.CallWS;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Filter extends AppCompatActivity implements View.OnClickListener{

    private EditText product;
    private TextView from_txt;
    private TextView to_txt;
    private Calendar date_from;
    private Calendar date_to;
    private InputMethodManager imm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        from_txt = (TextView) findViewById(R.id.filter_from_txt);
        to_txt = (TextView) findViewById(R.id.filter_to_txt);
        product = (EditText) findViewById(R.id.product);
        imm =  (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        Button from = (Button) findViewById(R.id.filter_from);
        Button to = (Button) findViewById(R.id.filter_to);
        Button submit = (Button) findViewById(R.id.filter);

        product.setOnFocusChangeListener(onFocusChangeListener);
        from.setOnClickListener(this);
        to.setOnClickListener(this);
        submit.setOnClickListener(this);

        Utils.createConnectivityListener(this);
    }

    @Override
    public void onClick(View v) {
        if ( v.getId() == R.id.filter ) {
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            new CallWS.SubmitGetResponse(Filter.this, from_txt.getText().toString(), to_txt.getText().toString(), product.getText().toString()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else if ( v.getId() == R.id.filter_from ) {
            getFrom();
        } else if ( v.getId() == R.id.filter_to ) {
            getTo();
        }
    }

    private void getFrom() {
        final Calendar currentDate = Calendar.getInstance();
        date_from = Calendar.getInstance();
        new DatePickerDialog(Filter.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                date_from.set(year, monthOfYear, dayOfMonth);
                new TimePickerDialog(Filter.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        date_from.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        date_from.set(Calendar.MINUTE, minute);
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ITALIAN);
                        from_txt.setText(format.format(date_from.getTime()));
                        Log.v(Login.class.getSimpleName(), "The choosen one " + date_from.getTime());
                    }
                }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show();
            }
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
    }

    private void getTo() {
        final Calendar currentDate = Calendar.getInstance();
        date_to = Calendar.getInstance();
        new DatePickerDialog(Filter.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                date_to.set(year, monthOfYear, dayOfMonth);
                new TimePickerDialog(Filter.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        date_to.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        date_to.set(Calendar.MINUTE, minute);
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ITALIAN);
                        to_txt.setText(format.format(date_to.getTime()));
                        Log.v(Login.class.getSimpleName(), "The choosen one " + date_to.getTime());
                    }
                }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show();
            }
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
    }

    private View.OnFocusChangeListener   onFocusChangeListener=  new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if( !product.hasFocus() ) {
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        }
    };

    @Override
    public void onDestroy(){
        super.onDestroy();
        try {
            this.unregisterReceiver(Utils.networkStateReceiver);
        } catch (Exception e){
            Log.e("ERROR", "Unable to unregister network receiver.");
        }
    }
}
