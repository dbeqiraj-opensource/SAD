package com.dbeqiraj.sad.activities.details.fragments;


import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.dbeqiraj.sad.R;
import com.dbeqiraj.sad.dbtools.DBHelper;
import com.dbeqiraj.sad.utilities.Utils;

public class Table extends Fragment {

    private TableLayout tableLayout;
    private LayoutInflater inflater;
    private ViewGroup container;
    private String code;

    public Table() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        this.inflater = inflater;
        this.container = container;
        View view = inflater.inflate(R.layout.fragment_table, container, false);
        code = getArguments().getString("code");
        tableLayout = (TableLayout) view.findViewById(R.id.table);
        setupTable(code);
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && getActivity() != null) {
            getActivity().setTitle(String.format(getString(R.string.title_details), code));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_details, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.pick_code) {
            pickCode();
        } else {
            getActivity().onBackPressed();
        }
        return false;
    }

    private void setupTable(String code){
        tableLayout.removeAllViews();
        Cursor c = Utils.getDataByCode(getActivity(), code);
        TableRow header = (TableRow) inflater.inflate(R.layout.table_row, container, false);
        TextView h_date = (TextView) header.findViewById(R.id.tr_date);
        TextView h_count = (TextView) header.findViewById(R.id.tr_count);
        h_date.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
        h_count.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
        h_date.setTypeface(Typeface.DEFAULT_BOLD);
        h_count.setTypeface(Typeface.DEFAULT_BOLD);
        tableLayout.addView(header);
        for ( int i=0; c.moveToNext(); i++ ) {
            TableRow tableRow = (TableRow) inflater.inflate(R.layout.table_row, container, false);
            Drawable roundDrawable = ContextCompat.getDrawable(getActivity(), R.drawable.table_cell);
            TextView tr_date = (TextView) tableRow.findViewById(R.id.tr_date);
            TextView tr_count = (TextView) tableRow.findViewById(R.id.tr_count);

            String color = ( i%2 == 0 ) ? "#ffdd9b" : "#edfcfb";
            roundDrawable.setColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_ATOP);
            if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                tr_date.setBackgroundDrawable(roundDrawable);
                tr_count.setBackgroundDrawable(roundDrawable);
            }
            else {
                tr_date.setBackground(roundDrawable);
                tr_count.setBackground(roundDrawable);
            }

            tr_date.setText(c.getString(c.getColumnIndex(DBHelper.TABLE_EA_FIELD_DATE)));
            tr_count.setText(c.getString(c.getColumnIndex(DBHelper.TABLE_EA_FIELD_CNTKLIENT)));
            tableLayout.addView(tableRow);
        }
        c.close();
    }

    private void pickCode(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Holo_Light_Dialog);
        dialog.setTitle(getString(R.string.pick_code));
        final CharSequence[] codes = getArguments().getCharSequenceArray("codes");
        dialog.setItems(codes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if ( codes != null ) {
                    code = codes[which].toString();
                    getActivity().setTitle(String.format(getString(R.string.title_details), code));
                    setupTable(code);
                }
            }
        });
        Dialog d = dialog.create();
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.show();
    }
}