package com.dbeqiraj.sad.activities;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.dbeqiraj.sad.R;
import com.dbeqiraj.sad.utilities.Utils;
import com.dbeqiraj.sad.utilities.ws.CallWS;

public class Login extends AppCompatActivity implements View.OnClickListener {

    private AutoCompleteTextView username;
    private EditText password;
    private InputMethodManager imm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username = (AutoCompleteTextView) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        imm =  (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        Button submit = (Button) findViewById(R.id.signin_submit);

        username.setOnFocusChangeListener(onFocusChangeListener);
        password.setOnFocusChangeListener(onFocusChangeListener);
        submit.setOnClickListener(this);

        Utils.createConnectivityListener(this);
    }

    @Override
    public void onClick(View v) {
        if ( v.getId() == R.id.signin_submit ) {
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            new CallWS.SubmitLogin(Login.this, username.getText().toString(), password.getText().toString()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        }
    }

    private View.OnFocusChangeListener   onFocusChangeListener=  new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if( !username.hasFocus() && !password.hasFocus() ) {
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        }
    };

    @Override
    public void onDestroy(){
        super.onDestroy();
        try {
            this.unregisterReceiver(Utils.networkStateReceiver);
        } catch (Exception e){
            Log.e("ERROR", "Unable to unregister network receiver.");
        }
    }
}