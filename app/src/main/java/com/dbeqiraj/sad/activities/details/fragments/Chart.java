package com.dbeqiraj.sad.activities.details.fragments;


import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.dbeqiraj.sad.R;
import com.dbeqiraj.sad.dbtools.DBHelper;
import com.dbeqiraj.sad.utilities.Utils;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class Chart extends Fragment  {

    private BarChart mChart;
    private String code;

    public Chart() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_chart, container, false);
        code = getArguments().getString("code");
        mChart = (BarChart) view.findViewById(R.id.chart);
        setupChart(code);
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && getActivity() != null) {
            getActivity().setTitle(String.format(getString(R.string.title_details), code));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_details, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.pick_code) {
            pickCode();
        } else {
            getActivity().onBackPressed();
        }
        return false;
    }

    private void pickCode(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Holo_Light_Dialog);
        dialog.setTitle(getString(R.string.pick_code));
        final CharSequence[] codes = getArguments().getCharSequenceArray("codes");
        dialog.setItems(codes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if ( codes != null ) {
                    code = codes[which].toString();
                    getActivity().setTitle(String.format(getString(R.string.title_details), code));
                    setupChart(code);
                }
            }
        });
        Dialog d = dialog.create();
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.show();
    }

    private void setupChart( String code ){
        mChart.clear();
        ArrayList<BarEntry> yValues = new ArrayList<>();
        final SparseArray<String> labels = new SparseArray<>();

        Cursor c = Utils.getDataByCode(getActivity(), code);
        for ( int i=0; c.moveToNext(); i++ ) {
            yValues.add(new BarEntry(i, c.getInt(c.getColumnIndex(DBHelper.TABLE_EA_FIELD_CNTKLIENT))));
            labels.put(i, c.getString(c.getColumnIndex(DBHelper.TABLE_EA_FIELD_DATE)));
        }
        c.close();

        BarDataSet set1 = new BarDataSet(yValues, code);

        set1.setDrawIcons(false);

        set1.setColors(ColorTemplate.COLORFUL_COLORS);


        BarData data = new BarData(set1);

        data.setValueTextSize(10f);
        //data.setBarWidth(0.1f);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(1f);
        xAxis.setGranularityEnabled(true);

        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                if ( value % 1 == 0 && labels.indexOfKey((int) value) != -1 ) {
                    return labels.get((int) value);
                } else {
                    return "";
                }
            }
        });
        mChart.animateY(1000);
        mChart.setData(data);
        mChart.setDescription(null);
    }
}
