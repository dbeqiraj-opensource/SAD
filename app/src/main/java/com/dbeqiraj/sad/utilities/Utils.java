package com.dbeqiraj.sad.utilities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.dbeqiraj.sad.R;
import com.dbeqiraj.sad.activities.details.Details;
import com.dbeqiraj.sad.dbtools.DBHelper;
import com.dbeqiraj.sad.dbtools.DBProvider;
import com.dbeqiraj.sad.model.RecordsRecordsEcuriaArtikullit;
import com.dbeqiraj.sad.utilities.ws.SetupWSCall;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by d.beqiraj on 5/1/2017.
 */

public class Utils {

    public static void showSimpleDialog(Context context, String title, String message, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", listener);
        builder.show();
    }

    public static void noConectivity(final Activity activity ) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(activity.getString(R.string.no_connectivity))
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        if ( Utils.getCodes(activity).length > 1 ) {
                            activity.startActivity(new Intent(activity, Details.class));
                        } else {
                            Toast.makeText(activity, activity.getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static Dialog showProgressDialog(Context context, String message) {
        ProgressDialog alertDialog = new ProgressDialog(context);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        return alertDialog;
    }

    public static boolean isNetworkOK;
    public static NetworkStateReceiver networkStateReceiver = new NetworkStateReceiver();

    public static void createConnectivityListener(final Activity thisActivity) {

        networkStateReceiver.connected = isNetworkOK;

        networkStateReceiver.setListener(new NetworkStateReceiver.NetworkStateReceiverListener() {
            @Override
            public void networkAvailable() {
                isNetworkOK = true;
                Log.d("NETWORK CHANGE", "THERE IS INTERNET CONNECTION");
            }

            @Override
            public void networkUnavailable() {
                isNetworkOK = false;
                Log.d("NETWORK CHANGE", "THERE IS NO INTERNET CONNECTION");
            }
        });
        thisActivity.registerReceiver(networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    public static void setCredentials(Activity activity, String username, String password) {
        DBProvider dbp = new DBProvider();
        dbp.setContext(activity);
        dbp.initializeMOpenHelper(activity);
        dbp.setTable_name(DBHelper.TABLE_USER);
        dbp.delete(DBProvider.CONTENT_URI, null, null);
        ContentValues cv = new ContentValues();
        cv.put(DBHelper.TABLE_USER_FIELD_USERNAME, username);
        cv.put(DBHelper.TABLE_USER_FIELD_PASSWORD, password);
        dbp.insert(DBProvider.CONTENT_URI, cv);
    }

    public static Cursor getCredentials(Activity activity) {
        DBProvider dbp = new DBProvider();
        dbp.setContext(activity);
        dbp.initializeMOpenHelper(activity);
        dbp.setTable_name(DBHelper.TABLE_USER);
        String[] columns = {"*"};

        return dbp.query(DBProvider.CONTENT_URI, columns, null, null, null);
    }

    public static class StoreData extends AsyncTask<Void, Void, Void> {

        Dialog dialog;
        DBProvider dbp;
        Activity activity;

        public StoreData(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = showProgressDialog(activity, activity.getString(R.string.store_data));
            dialog.show();
            dbp = new DBProvider();
            dbp.setContext(activity);
            dbp.initializeMOpenHelper(activity);
            dbp.setTable_name(DBHelper.TABLE_EA);
        }

        @Override
        protected Void doInBackground(Void... params) {
            dbp.delete(DBProvider.CONTENT_URI, null, null);
            for (RecordsRecordsEcuriaArtikullit ecuriaArtikullit : SetupWSCall.reportsResponse.getRecords().getEcuriaArtikullit()) {
                ContentValues cv = new ContentValues();
                cv.put(DBHelper.TABLE_EA_FIELD_CNTKLIENT,          ecuriaArtikullit.getCntKlient());
                cv.put(DBHelper.TABLE_EA_FIELD_DATE,         ecuriaArtikullit.getDt());
                cv.put(DBHelper.TABLE_EA_FIELD_KODIART,      ecuriaArtikullit.getKArt());
                dbp.insert(DBProvider.CONTENT_URI, cv);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            dialog.dismiss();
            activity.startActivity(new Intent(activity, Details.class));
        }
    }

    public static Cursor getDataByCode(Activity activity, String code) {
        DBProvider dbp = new DBProvider();
        dbp.setContext(activity);
        dbp.initializeMOpenHelper(activity);
        dbp.setTable_name(DBHelper.TABLE_EA);
        String[] columns = {"*"};
        String selection = DBHelper.TABLE_EA_FIELD_KODIART + " = ?";
        String[] selectionArgs = {code};

        return dbp.query(DBProvider.CONTENT_URI, columns, selection, selectionArgs, null);
    }

    public static CharSequence[] getCodes(Activity activity) {
        DBProvider dbp = new DBProvider();
        dbp.setContext(activity);
        dbp.initializeMOpenHelper(activity);
        dbp.setTable_name(DBHelper.TABLE_EA);
        String[] columns = new String[]{"DISTINCT " + DBHelper.TABLE_EA_FIELD_KODIART};

        Cursor c = dbp.query(DBProvider.CONTENT_URI, columns, null, null, null);
        List<String> codes = new ArrayList<>();
        if (c != null) {
            while (c.moveToNext()) {
                codes.add(c.getString(c.getColumnIndex(DBHelper.TABLE_EA_FIELD_KODIART)));
            }
            c.close();
        }
        return codes.toArray(new String[codes.size()]);
    }
}