package com.dbeqiraj.sad.utilities;

/**
 * Created by d.beqiraj on 5/1/2017.
 */

public class Constant {

    public static final String API_URL = "http://185.158.1.25:8882/";
    public static final String DB_NAME = "sad_db";
    public final static int TIMEOUT = 10  *   1000;
}
