package com.dbeqiraj.sad.utilities.ws;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.widget.Toast;

import com.dbeqiraj.sad.R;
import com.dbeqiraj.sad.activities.Filter;
import com.dbeqiraj.sad.activities.details.Details;
import com.dbeqiraj.sad.dbtools.DBHelper;
import com.dbeqiraj.sad.model.Request;
import com.dbeqiraj.sad.model.RequestList;
import com.dbeqiraj.sad.utilities.Constant;
import com.dbeqiraj.sad.utilities.Utils;

import static com.dbeqiraj.sad.utilities.Utils.showProgressDialog;

/**
 * Created by d.beqiraj on 5/1/2017.
 */

public class CallWS {

    public static class SubmitLogin extends AsyncTask< Void, Void, Void> {

        Dialog dialog;
        Activity activity;
        boolean timeout;
        private String username;
        private String password;

        public SubmitLogin(Activity activity, String username, String password) {
            this.activity = activity;
            this.username = username;
            this.password = password;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = showProgressDialog(activity, activity.getString(R.string.loging_in));
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            timeout = true;
            long start = System.currentTimeMillis();
            SetupWSCall.getToken(activity, username, password);
            while (System.currentTimeMillis() - start < Constant.TIMEOUT) {
                if (SetupWSCall.tokenResponse != null) {
                    timeout = false;
                    break;
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            dialog.dismiss();
            if ( timeout ) {
                Utils.noConectivity(activity);
            } else if ( SetupWSCall.tokenOK == 1 ) {
                Utils.setCredentials(activity, "tino", "12345");
                activity.startActivity(new Intent(activity, Filter.class));
                activity.finish();
            }
        }
    }

    private static class RefreshToken extends AsyncTask< Void, Void, Void> {

        Dialog dialog;
        Activity activity;
        boolean timeout;
        String username;
        String password;

        RefreshToken(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = showProgressDialog(activity, activity.getString(R.string.refresh_token));
            dialog.show();
            Cursor c = Utils.getCredentials(activity);
            if ( c.moveToNext() ) {
                username = c.getString(c.getColumnIndex(DBHelper.TABLE_USER_FIELD_USERNAME));
                password = c.getString(c.getColumnIndex(DBHelper.TABLE_USER_FIELD_PASSWORD));
            }
            c.close();
        }

        @Override
        protected Void doInBackground(Void... params) {
            timeout = true;
            long start = System.currentTimeMillis();
            SetupWSCall.getToken(activity, username, password);
            while (System.currentTimeMillis() - start < Constant.TIMEOUT) {
                if (SetupWSCall.tokenOK != 0) {
                    timeout = false;
                    break;
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            dialog.dismiss();
            if ( timeout ) {
                Utils.showSimpleDialog(activity, activity.getString(R.string.app_name), activity.getString(R.string.connection_error), null);
            } else if ( SetupWSCall.tokenOK == 1 ) {
                activity.startActivity(new Intent(activity, Details.class));
            }
        }
    }

    public static class SubmitGetResponse extends AsyncTask< Void, Void, Void> {

        Dialog dialog;
        Activity activity;
        boolean timeout;
        String from;
        String to;
        String product;

        public SubmitGetResponse(Activity activity, String from, String to, String product) {
            this.activity = activity;
            this.from = from;
            this.to = to;
            this.product = product;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = showProgressDialog(activity, activity.getString(R.string.get_data));
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            timeout = true;
            long start = System.currentTimeMillis();


            RequestList[] reportRequest = new RequestList[1];
            reportRequest[0] = new RequestList();
            reportRequest[0].setClientID(-1);
            reportRequest[0].setDays(0);
            //reportRequest[0].setFrom("2017-03-11 19:54:11");
            reportRequest[0].setFrom(from);
            reportRequest[0].setMonthID(0);
            //reportRequest.setProductCode(product);
            reportRequest[0].setProductCode("CR910010,CR910011,CR910012");
            reportRequest[0].setReportName("EcuriaArtikullit");
           // reportRequest[0].setTo("2017-04-10 19:54:11");
            reportRequest[0].setTo(to);
            reportRequest[0].setUserID(-1);

            Request request = new Request();
            request.setList(reportRequest);

            SetupWSCall.getReports(activity, request);
            while (System.currentTimeMillis() - start < Constant.TIMEOUT) {
                if (SetupWSCall.reportsOK != 0) {
                    timeout = false;
                    break;
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            dialog.dismiss();
            if ( timeout ) {
                Utils.noConectivity(activity);
            } else if ( SetupWSCall.reportsOK == 1 ) {
                new Utils.StoreData(activity).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else if ( SetupWSCall.reportsMsg.contains("Invalid") ) {
                new RefreshToken(activity).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else{
                Toast.makeText(activity, activity.getString(R.string.nothing_found), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
