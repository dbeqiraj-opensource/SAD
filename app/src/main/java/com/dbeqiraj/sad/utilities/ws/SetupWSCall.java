package com.dbeqiraj.sad.utilities.ws;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.dbeqiraj.sad.R;
import com.dbeqiraj.sad.http_rest.ReportsApiClient;
import com.dbeqiraj.sad.http_rest.TokenApiClient;
import com.dbeqiraj.sad.model.Records;
import com.dbeqiraj.sad.model.Request;
import com.dbeqiraj.sad.model.TokenResponse;
import com.dbeqiraj.sad.utilities.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by d.beqiraj on 5/1/2017.
 */

public class SetupWSCall {

    public static TokenResponse tokenResponse;
    static int tokenOK;
    static void getToken(final Activity activity, String username, String password) {
        tokenResponse =   null;
        tokenOK = 0;
        if ( Utils.isNetworkOK ) {
            try {
                Call<TokenResponse> call = TokenApiClient.getClient().getToken();

                call.enqueue(new Callback<TokenResponse>() {
                    @Override
                    public void onFailure(Call<TokenResponse> call, Throwable t) {
                        Log.e("APIPlug (getToken)", "Error Occured: " + t.getMessage());
                    }

                    @Override
                    public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                        Log.d("APIPlug (getToken)", "Successfully response fetched" );
                        tokenResponse = response.body();
                        tokenOK = ( tokenResponse.getKey() != null &&
                                !tokenResponse.getKey().equals("") &&
                                tokenResponse.getLink() != null &&
                                !tokenResponse.getLink().equals("") ) ? 1 : 2;
                    }
                });
            } catch ( Exception e ) {
                e.printStackTrace();
                Toast.makeText(activity, activity.getString(R.string.error), Toast.LENGTH_SHORT).show();
            }

        }
    }

    public static Records reportsResponse;
    static String reportsMsg;
    static int reportsOK;
    static void getReports(final Activity activity, Request request) {
        reportsResponse =   null;
        reportsOK = 0;
        if ( Utils.isNetworkOK ) {
            try {
                Call<Records> call = ReportsApiClient.getClient().getReports(request);

                call.enqueue(new Callback<Records>() {
                    @Override
                    public void onFailure(Call<Records> call, Throwable t) {
                        Log.e("APIPlug (getReports)", "Error Occured: " + t.getMessage());
                    }

                    @Override
                    public void onResponse(Call<Records> call, Response<Records> response) {
                        Log.d("APIPlug (getReports)", "Successfully response fetched" );
                        reportsMsg = response.message();
                        reportsResponse = response.body();
                        reportsOK = ( reportsResponse != null &&
                                        reportsResponse.getRecords() != null &&
                                        reportsResponse.getRecords().getEcuriaArtikullit() != null &&
                                        reportsResponse.getRecords().getEcuriaArtikullit().length > 0 ) ? 1 : 2;
                    }
                });
            } catch ( Exception e ) {
                e.printStackTrace();
                Toast.makeText(activity, activity.getString(R.string.error), Toast.LENGTH_SHORT).show();
            }

        }
    }
}