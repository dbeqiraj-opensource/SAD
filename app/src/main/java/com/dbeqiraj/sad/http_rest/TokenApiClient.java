package com.dbeqiraj.sad.http_rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.dbeqiraj.sad.utilities.Constant.API_URL;

/**
 * Created by d.beqiraj on 5/1/2017.
 */

public class TokenApiClient {
    private static APIPlug REST_CLIENT;

    static {
        setupRestClient();
    }

    private TokenApiClient() {}

    public static APIPlug getClient() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header("DeviceID", "test")
                        .header("SerialNumber", "test")
                        .header("SalesForceCode", "test")
                        .header("Password", "12345")
                        .header("CompanyCode", "test")
                        .header("Username", "tino")
                        .header("SalesForceID", "test")
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())
                .build();

        REST_CLIENT = retrofit.create(APIPlug.class);
    }
}
