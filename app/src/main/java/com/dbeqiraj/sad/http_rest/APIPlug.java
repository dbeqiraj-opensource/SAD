package com.dbeqiraj.sad.http_rest;

import com.dbeqiraj.sad.model.Records;
import com.dbeqiraj.sad.model.Request;
import com.dbeqiraj.sad.model.TokenResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by d.beqiraj on 5/1/2017.
 */

public interface APIPlug {

    @POST("auth/")
    Call<TokenResponse> getToken();

    @POST("reports/getreports/")
    Call<Records> getReports(@Body Request request);

}