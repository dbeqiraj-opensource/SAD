package com.dbeqiraj.sad.dbtools;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.dbeqiraj.sad.utilities.Constant;

/**
 * Created by d.beqiraj on 5/1/2017.
 */

public class DBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION                   = 1;
    private static final String DB_NAME                         = Constant.DB_NAME;

    public static final String TABLE_EA                     = "ecuariaartikullit";
    public static final String TABLE_EA_FIELD_CNTKLIENT     = "CNTKLIENT";
    public static final String TABLE_EA_FIELD_DATE          = "DATE";
    public static final String TABLE_EA_FIELD_KODIART       = "KODIART";

    public static final String TABLE_USER                   = "user";
    public static final String TABLE_USER_FIELD_USERNAME    = "USERNAME";
    public static final String TABLE_USER_FIELD_PASSWORD    = "PASSWORD";

    private static final String CREATE_TABLE_USER = "CREATE TABLE IF NOT EXISTS "
            + TABLE_USER
            + " ("
            + TABLE_USER_FIELD_USERNAME + " TEXT NOT NULL, "
            + TABLE_USER_FIELD_PASSWORD   + " TEXT NOT NULL"
            +")";

    private static final String CREATE_TABLE_EA = "CREATE TABLE IF NOT EXISTS "
            + TABLE_EA
            + " ("
            + TABLE_EA_FIELD_CNTKLIENT + " INTEGER NOT NULL, "
            + TABLE_EA_FIELD_DATE      + " TEXT NOT NULL, "
            + TABLE_EA_FIELD_KODIART   + " TEXT NOT NULL"
            +")";

    DBHelper(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(CREATE_TABLE_USER);
        db.execSQL(CREATE_TABLE_EA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EA);
        onCreate(db);
    }
}
